var btnLogout = document.getElementById("btnLogout");
var select = document.getElementById("selectDot");
var btnShow = document.getElementById("btnShow");
var btnAdd = document.getElementById("btnAdd");
var btnEdit = document.getElementById("btnEdit");
var btnSave = document.getElementById("btnSave"); 
var btnChangePass = document.getElementById("btnChangePass");

function successAjaxShow(result){
    if(result['result']=='error'){
        alert(result['message']);
        return;
    }
    Array.from(document.getElementsByClassName("inputReadOnly")).forEach(element => {
        $(element).css({"display":"flex"});
    });
    Array.from(document.getElementsByClassName("inputDiemTuNhap")).forEach(element => {
        $(element).attr("readonly",true);
    });
    $("#btnAdd").attr("disabled",result['dataBtn']['btnAdd']==0);
    $("#btnEdit").attr("disabled",result['dataBtn']['btnEdit']==0);
    var result = JSON.parse(result['data']);
    var maNoiDung = Object.keys(result);
    maNoiDung.forEach(element=>{
        $("#input-"+element).val(result[element]['TN']);
        $("#input-DVC-"+element).val(result[element]['DVC']);
        $("#input-DPT-"+element).val(result[element]['PT']);

    });
    
}

function successAjaxSave(result){
    alert(result['message']);
    btnShow.click();
}

btnShow.addEventListener("click",()=>{
    $("#btnSave").css({"display":"none"});
    if(select.value==''){
        alert("Vui lòng chọn đợt !!!!");
        return;
    }
    else{
        $.ajax({
            url:"chamdiemAjax.php",
            type:"post",
            dataType:"json",
            data:{
                func:"show",
                data:select.value,
            },
            success:function(result){
                successAjaxShow(result);
            }
        });
        return;
    }
});

btnLogout.onclick = ()=>{
    document.getElementById("frmLogout").submit();
};

btnAdd.onclick = ()=>{
    Array.from(document.getElementsByClassName("inputReadOnly")).forEach(element => {
        $(element).css({"display":"none"});
    });
    
    Array.from(document.getElementsByClassName("inputDiemTuNhap")).forEach(element => {
        $(element).attr("readonly",false);
    });
    $("#btnSave").css({"display":"block"});

};

btnEdit.onclick = ()=>{
    Array.from(document.getElementsByClassName("inputReadOnly")).forEach(element => {
        $(element).css({"display":"none"});
    });
    
    Array.from(document.getElementsByClassName("inputDiemTuNhap")).forEach(element => {
        $(element).attr("readonly",false);
    });
    $("#btnSave").css({"display":"block"});
    
};

btnSave.onclick = ()=>{
    if(select.value==''){
        alert("Vui lòng chọn đợt !!!!");
        return;
    }
    else{
        var funcName="";
        var dataObject={};
        if($("#btnAdd").attr("disabled")){
            funcName = "edit";
            
        }else{
            funcName = "add";
        }
        Array.from($(".inputDiemTuNhap")).forEach(element=>{
            dataObject[$(element).attr("id").split("-")[1]]=$(element).val();
        });
        dataObject['maDot']=select.value;
        $.ajax({
            url:"chamdiemAjax.php",
            type:"post",
            dataType:"json",
            data:{
                func:funcName,
                data:dataObject,
            },
            success:function(result){
                successAjaxSave(result);
            }
        });
        return;
    }
}

btnChangePass.onclick = ()=>{
    document.getElementById("frmChangePass").submit();
;}

window.onload = ()=>{
    Array.from(document.getElementsByClassName("inputDiemTuNhap")).forEach(element => {
        element.addEventListener("keyup",()=>{
            if(parseInt(element.value)>parseInt(element.getAttribute("max"))){
                element.value = element.getAttribute("max");
            }
        });
    });
};