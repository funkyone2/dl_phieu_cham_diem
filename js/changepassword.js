var btnLogout = document.getElementById("btnLogout");
var btnCancel = document.getElementById("btnCancel");
var btnSave = document.getElementById("btnSave");

btnLogout.onclick = ()=>{
    document.getElementById("frmLogout").submit();
};

btnCancel.onclick = ()=>{
    document.getElementById("frmCancel").submit();
}

btnSave.onclick = ()=>{
    var pass = document.getElementById("inputPass");
    var passNew = document.getElementById("inputPassNew");
    var passNewAgain = document.getElementById("inputPassNewAgain");
    var flag = 1;
    if(pass.value == ''){
        alert("Không được bỏ trống mật khẩu cũ !!!!");
        flag = 0;
        return;
    }
    if(passNew.value == ''){
        alert("Không được bỏ trống mật khẩu mới !!!!");
        flag = 0;
        return false;
    }
    if(passNew.value != passNerAgain.value){
        alert("Mật khẩu mới và mật khẩu nhập lại không trùng nhau !!!");
        flag = 0;
        return false;
    }
    if(flag==1){
        document.getElementById("frmSave").submit();
    }
}