<?php 
header('Content-Type: text/html; charset=utf-8');
class DB{
    private $serverName = ".\SQLEXPRESS";

    private $connectInfo = array(
        "Database"=>"VHDN-PCBG",
        "UID"=>"sa",
        "PWD"=>"vitinh@123",
        "CharacterSet" => "UTF-8"  
    );

    private  $conn;
    
    private static $instance;

    private function __construct(){
        $this->conn = sqlsrv_connect( $this->serverName, $this->connectInfo);
        /*if($this->conn == false){
            echo "Không connect được";
            var_dump($this->conn);
        }
        else{
            echo "Connect thành công";
            var_dump($this->conn);
        }
        */
    }

    function __desctruct(){
        if(isset($this->conn)){
            sqlsrv_close($this->conn);
        }
    }

    public static function getInstance(){
        if(!isset(static::$instance)){
            static::$instance = new DB();
        }
        return static::$instance;
    }

    public function exec($sql,$arr = array()){
        $stmt = sqlsrv_prepare($this->conn,$sql,$arr);
        return sqlsrv_execute($stmt);  
    }

    public function query($sql,$arr = array()){
        $result = sqlsrv_query($this->conn,$sql,$arr,array( "Scrollable" => SQLSRV_CURSOR_KEYSET ));
        $return = array();
        if($result===false){
            $return['success'] = false;
            return $return;
        }else{
            $return['success'] = true;
        }
        $return['row_count'] = sqlsrv_num_rows($result);
        $return['field_count'] = sqlsrv_num_fields($result);
        $return['metadata'] = sqlsrv_field_metadata($result);
        $data = array();
        $index = 0;
        while($row = sqlsrv_fetch_array($result,SQLSRV_FETCH_ASSOC)){
            $data[$index++] = $row;
        }
        $return['data']=$data;
        return $return;
    }
}
?>