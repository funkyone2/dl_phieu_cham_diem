<?php 
     if(!isset($_SESSION)){
        session_start();
      }
    include_once("./DB.php");
    $arrInfoUser = array(
        $_POST['username'],
        $_POST['password']
    );
    $sql = "select MaHRMS,Password,NghiHuu,TKCaNhan from TabCBCNV where MaHRMS=? and Password=?";
    $result = DB::getInstance()->query($sql,$arrInfoUser);
    if($result['row_count']>0){
        $_SESSION['maHR'] = $result['data'][0]['MaHRMS'];
        //Get info user:
        $sql = "select HoTen,TenDonViCap3,TenDonViCap4 from ViewCBCNV where MaHRMS = ?";
        $result_info = DB::getInstance()->query($sql,array($_SESSION['maHR']));
        if($result_info['row_count']>0){
            $_SESSION['infoUser'] = $result_info['data'][0];
        }
        else{
            $_SESSION['infoUser'] = array(
                "HoTen"=>"",
                "TenDonViCap3"=>"Công ty Điện Lực",
                "TenDonViCap4"=>"",
            );
        }

        if($result['data'][0]['NghiHuu']==1){
            //Báo lỗi đã nghỉ hưu
            $_SESSION['error_info'] = "Tài khoản này đã nghỉ hưu";
            header("Location:./error.php");
        }
        if($result['data'][0]['TKCaNhan']==0){
            //Báo lỗi không phải tk cá nhân
            $_SESSION['error_info'] = "Tài khoản này không phải tài khoản cá nhân";
            header("Location:./error.php");
        }

        
        header("Location:./chamdiem.php");
    }
    else{
        //Báo lỗi mã HR hoặc mật khẩu không chính xác
        $_SESSION['error_info'] = "Mã HR hoặc mật khẩu không chính xác";

        $_SESSION['infoUser'] = array(
            "HoTen"=>"",
            "TenDonViCap3"=>"Công ty Điện Lực",
            "TenDonViCap4"=>"Công ty Điện Lực",
        );

        header("Location:./error.php");
    }
?>