<?php

include_once("./chamdiemController.php");
?>
<!DOCTYPE html>
<html>
<?php
    include_once("./head.php");
?>
<link rel="stylesheet" href="../css/chamdiem.css">

<body>
    <nav class="navbar navbar-light bg-light">
        <span class="navbar-brand mb-0 h1">
            <?php
                echo $_SESSION['infoUser']["TenDonViCap3"] . " - " . $_SESSION['infoUser']['TenDonViCap4'];
            ?>
        </span>
        <div class="btn-group">
            <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <?php echo $_SESSION['maHR']; ?>
            </button>
            <div class="dropdown-menu dropdown-menu-right dropdown-custome">
                <button class="dropdown-item" type="button" id="btnChangePass">Đổi mật khẩu</button>
                <button class="dropdown-item" type="button" id="btnLogout">Đăng xuất</button>
            </div>
        </div>
    </nav>
    <h2 class="text-center mt-3">Bảng chấm thi văn hoá doanh nghiệp</h2>
    <div class="text-center"><?php echo $_SESSION['infoUser']['HoTen'] ?></div>
    <div class="container mt-3">
        <div class="row justify-content-center mb-6">
            <select class="custom-select mr-2 custom-select-user" id="selectDot">
                <option value="" selected>Chọn đợt...</option>
                <?php
                foreach ($arrDot as $dot) {
                    echo "<option value='" . $dot['MaDot'] . "'>" . $dot['MaDot'] . "</option>";
                }
                ?>
            </select>

            <button type="button" class="btn btn-success mr-2" id="btnShow">Xem</button>
            <button type="button" class="btn btn-primary mr-2" id="btnAdd" disabled>Thêm</button>
            <button type="button" class="btn btn btn-warning mr-2" id="btnEdit" disabled>Sửa</button>
        </div>
        <!-- Nội dung -->
        <!-- <div class="row mt-3">
            <div class="div-content">
                <div class="div-content-title"><h5>Nội dung kjsadlfjsahgkdhf;jaslfjdsalfjlkdsgdsafhdsfhsdfsadfsdfdsfdsfsdfdsfdsfasdfdsfdsaf</h5></div>
                <div class="form-group row">
                    
                    <label for="inputEmail3" class="col-sm-6 col-form-label">Điểm chuẩn</label>
                    <div class="col-sm-6">
                        <input type="number" class="form-control" id="inputEmail3" placeholder="Điểm chuẩn" readonly>
                    </div>
                   
                    <label for="inputEmail4" class="col-sm-6 col-form-label">Điểm tự nhập</label>
                    <div class="col-sm-6">
                        <input type="number" class="form-control insert" id="inputEmail4" placeholder="Điểm chuẩn">
                    </div>
                    
                    <label for="inputEmail4" class="col-sm-6 col-form-label">Điểm chuẩn</label>
                    <div class="col-sm-6">
                        <input type="number" class="form-control show" id="inputEmail4" placeholder="Điểm chuẩn" readonly>
                    </div>
                    
                    <label for="inputEmail4" class="col-sm-6 col-form-label">Điểm chuẩn</label>
                    <div class="col-sm-6">
                        <input type="number" class="form-control show" id="inputEmail4" placeholder="Điểm chuẩn" readonly>
                    </div>
                </div>
            </div>
        </div> -->

    </div>
    <div class="container mt-3">
        <?php
        echo $html;
        ?>
        <div class="row justify-content-center mt-3 mb-5">
            <button id="btnSave" class="btn btn-primary">Lưu</button>
        </div>
    </div>
</body>
<form id="frmLogout" action="./backerror.php" method="post"></form>
<form id="frmChangePass" action="./changepassword.php" method="post"></form>
<?php include_once("js_include.php"); ?>
<script style="text/javascript" src="../js/chamdiem.js"></script>

</html>