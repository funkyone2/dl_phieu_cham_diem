<?php
    if(!isset($_SESSION)){
        session_start();
    }
    include_once("./DB.php");

    function showHandler($data){
        if($data==""){
            return json_encode(array(
                "result"=>"error",
                "message"=>"Vui lòng chọn mã đợt !!!!"
            ));
        }

        //Get điểm cá nhân tự chấm và mã nội dung
        $sql = "select * from TabDiemCaNhanTuCham where MaHRMS = ? and MaDot = ?";
        $result = DB::getInstance()->query($sql,array($_SESSION['maHR'],$data));
        $arrMaNoiDung = array();
        foreach($result['metadata'] as $field){
            if(substr($field['Name'],0,1)=='B'){
                $arrMaNoiDung[] = $field['Name'];
            }
        }
        $diemTuCham = isset($result['data'][0])?$result['data'][0]:array();

        //Get điểm đơn vị chấm
        $sql = "select * from TabDiemCaNhanTTDVCham where MaHRMS = ? and MaDot = ?";
        $result = DB::getInstance()->query($sql,array($_SESSION['maHR'],$data));
        $diemDonViCham = isset($result['data'][0])?$result['data'][0]:array();

        //Get điểm phúc tra
        $sql = "select * from TabDiemCaNhanPhucTra where MaHRMS = ? and MaDot = ?";
        $result = DB::getInstance()->query($sql,array($_SESSION['maHR'],$data));
        $diemPhucTra = isset($result['data'][0])?$result['data'][0]:array();

        //Process data
        $dataReturn = array();
        foreach($arrMaNoiDung as $maNoiDung){
            $dataReturn[$maNoiDung] = array(
                "TN"=>isset($diemTuCham[$maNoiDung])?$diemTuCham[$maNoiDung]:"",
                "DVC"=>isset($diemDonViCham[$maNoiDung])?$diemDonViCham[$maNoiDung]:"",
                "PT"=>isset($diemPhucTra[$maNoiDung])?$diemPhucTra[$maNoiDung]:""
            ); 
        }

        //Kiểm tra date
        $sql = "select NgayDauCaNhan,NgayCuoiCaNhan,DaKhoa from TabDotChamCong where MaDot = ?";
        $result = DB::getInstance()->query($sql,array($data));
        $khoa = $result['data'][0]['DaKhoa'];
        $btnAdd = 0;
        $btnEdit = 0;
        if($khoa == 1){
            $btnAdd = 0;
            $btnEdit = 0;
        }else{
            //Ngay dau
            $ngayDau = $result['data'][0]['NgayDauCaNhan'];
            $timeNgayDau = ((array)$ngayDau)['date'];
            $timeNgayDau = strtotime($timeNgayDau);
            $timeNgayDau = date("Y-m-d",$timeNgayDau);
            //Ngay cuoi
            $ngayCuoi = $result['data'][0]['NgayCuoiCaNhan'];
            $timeNgayCuoi = strtotime(((array)$ngayCuoi)['date']);
            $timeNgayCuoi = date("Y-m-d",$timeNgayCuoi);
            $now = date("Y-m-d");
            if($now >= $timeNgayDau && $now <= $timeNgayCuoi){
                if(empty($diemTuCham)){
                    $btnAdd = 1;
                    $btnEdit = 0;
                }else{
                    $btnAdd = 0;
                    $btnEdit = 1;
                }
            }
        }        
        $dataBtn = array(
            "btnAdd"=>$btnAdd,
            "btnEdit"=>$btnEdit
        );

        //return
        return json_encode(array(
            "result"=>"success",
            "message"=>"Lấy data thành công",
            "data"=>json_encode($dataReturn),
            "dataBtn"=>$dataBtn
        ));
    }

    function addHandler($data){
        if($data==""){
            return json_encode(array(
                "result"=>"error",
                "message"=>"Không có dữ liệu"
            ));
        }

        //Check time 
        $sql = "select NgayDauCaNhan,NgayCuoiCaNhan,DaKhoa from TabDotChamCong where MaDot = ?";
        $result = DB::getInstance()->query($sql,array($data['maDot']));
        $khoa = $result['data'][0]['DaKhoa'];
        if($khoa == "1"){
            return json_encode(array(
                "result"=>"error",
                "message"=>"Đợt đã khoá !!!!"
            ));
        }else{
            //Ngay dau
            $ngayDau = $result['data'][0]['NgayDauCaNhan'];
            $timeNgayDau = ((array)$ngayDau)['date'];
            $timeNgayDau = strtotime($timeNgayDau);
            $timeNgayDau = date("Y-m-d",$timeNgayDau);
            //Ngay cuoi
            $ngayCuoi = $result['data'][0]['NgayCuoiCaNhan'];
            $timeNgayCuoi = strtotime(((array)$ngayCuoi)['date']);
            $timeNgayCuoi = date("Y-m-d",$timeNgayCuoi);
            $now = date("Y-m-d");
            if($now < $timeNgayDau){
                return json_encode(array(
                    "result"=>"error",
                    "message"=>"Chưa đến đợt chấm !!!!!"
                ));
            }
            if($now > $timeNgayCuoi){
                return json_encode(array(
                    "result"=>"error",
                    "message"=>"Đã qua đợt chấm !!!!!"
                ));
            }
        }        

        //Get mã nội dung
        $sql = "select * from TabTenMucCaNhan";
        $result = DB::getInstance()->query($sql);
        $arrMaNoiDung = array();
        foreach($result['metadata'] as $field){
            if(substr($field['Name'],0,1)=='B'){
                $arrMaNoiDung[] = $field['Name'];
            }
        }

        //insert
        $nameInsert = "(MaDot,MaHRMS";
        $dataInsert = "";
        $dataInsert.= "('".$data['maDot']."'";
        $dataInsert.= ",'".$_SESSION['maHR']."'";
        foreach($arrMaNoiDung as $maNoiDung){
            $dataInsert.= ",'".$data[$maNoiDung]."'";
            $nameInsert.=",".$maNoiDung;
        }
        $dataInsert.= ",'".date("Y-m-d")."')";
        $nameInsert.=",DateAdd)";
        
        $sql = "insert TabDiemCaNhanTuCham ".$nameInsert." values ".$dataInsert;
        $result = DB::getInstance()->exec($sql);
        $sql = "insert TabDiemCaNhanTTDVCham ".$nameInsert." values ".$dataInsert;
        $result = DB::getInstance()->exec($sql);

        return json_encode(array(
            "result"=>"success",
            "message"=>"Thêm thành công !!!!!"
        ));
    }
     
    function editHandler($data){
        if($data==""){
            return json_encode(array(
                "result"=>"error",
                "message"=>"Không có dữ liệu"
            ));
        }

        //Check time 
        $sql = "select NgayDauCaNhan,NgayCuoiCaNhan,DaKhoa from TabDotChamCong where MaDot = ?";
        $result = DB::getInstance()->query($sql,array($data['maDot']));
        $khoa = $result['data'][0]['DaKhoa'];
        if($khoa == "1"){
            return json_encode(array(
                "result"=>"error",
                "message"=>"Đợt đã khoá !!!!"
            ));
        }else{
            //Ngay dau
            $ngayDau = $result['data'][0]['NgayDauCaNhan'];
            $timeNgayDau = ((array)$ngayDau)['date'];
            $timeNgayDau = strtotime($timeNgayDau);
            $timeNgayDau = date("Y-m-d",$timeNgayDau);
            //Ngay cuoi
            $ngayCuoi = $result['data'][0]['NgayCuoiCaNhan'];
            $timeNgayCuoi = strtotime(((array)$ngayCuoi)['date']);
            $timeNgayCuoi = date("Y-m-d",$timeNgayCuoi);
            $now = date("Y-m-d");
            if($now < $timeNgayDau){
                return json_encode(array(
                    "result"=>"error",
                    "message"=>"Chưa đến đợt chấm !!!!!"
                ));
            }
            if($now > $timeNgayCuoi){
                return json_encode(array(
                    "result"=>"error",
                    "message"=>"Đã qua đợt chấm !!!!!"
                ));
            }
        }        

        //Get mã nội dung
        $sql = "select * from TabTenMucCaNhan";
        $result = DB::getInstance()->query($sql);
        $arrMaNoiDung = array();
        foreach($result['metadata'] as $field){
            if(substr($field['Name'],0,1)=='B'){
                $arrMaNoiDung[] = $field['Name'];
            }
        }

        //insert
        $updateString = "";
        foreach($arrMaNoiDung as $maNoiDung){
            $updateString.=$maNoiDung." = '".$data[$maNoiDung]."',";
        }
        $updateString.="DateEdit = '".date('Y-m-d')."'";

        
        $sql = "UPDATE TabDiemCaNhanTuCham SET ".$updateString." where MaDot='".$data['maDot']."' and MaHRMS='".$_SESSION['maHR']."'";
        $result = DB::getInstance()->exec($sql);

        return json_encode(array(
            "result"=>"success",
            "message"=>"Sửa thành công !!!!!"
        ));
    }

    if(isset($_SESSION['maHR'])){
        if(isset($_POST['func'])){
            switch ($_POST['func']){
                case "show":
                    die(showHandler($_POST['data']));
                    break;
                case "add":
                    die(addHandler($_POST['data']));
                    break;
                case "edit":
                    die(editHandler($_POST['data']));
                    break;
                default:
                die(json_encode(array(
                    "result"=>"error",
                    "message"=>"Không tìm thấy func phù hợp !!!"
                )));
                    break;
            }
        }
        else{
            die(json_encode(array(
                "result"=>"error",
                "message"=>"Không có func !!!!"
            )));
        }
    
    }
    else{
        die(json_encode(array(
            "result"=>"error",
            "message"=>"Chưa đăng nhập"
        )));
    }
