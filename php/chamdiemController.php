<?php
if(!isset($_SESSION)){
    session_start();
  }
if (!isset($_SESSION['maHR'])) {
    header("Location:../");
}
include_once("./DB.php");
//Mã đợt
$sql = "select MaDot from TabDotChamCong";
$result = DB::getInstance()->query($sql);
$arrDot = $result['data'];

//$metadata = substr($result['metadata'][0]['Name'],0,1);
//Tên nội dung, nội dung
$sql = "select * from TabTenMucCaNhan";
$result = DB::getInstance()->query($sql);
$arrMaNoiDung = array();
foreach($result['metadata'] as $field){
    if(substr($field['Name'],0,1)=='B'){
        $arrMaNoiDung[] = $field['Name'];
    }
}
$arrNoiDung = $result['data'][0];

//Get trọng số điểm
$sql = "select * from TabTrongSoDiemCaNhan";
$result = DB::getInstance()->query($sql);
$arrTrongSoDiem = $result['data'][0];

//Get hướng dẫn
$sql = "select * from TabHuongDanCaNhan";
$result = DB::getInstance()->query($sql);
$arrHuongDan = $result['data'][0];


//Gen code html
$html = "";
$stt = 1;
foreach($arrMaNoiDung as $maNoiDung){
    if($arrHuongDan[$maNoiDung]!=''){
        $arrHuongDan[$maNoiDung] = "( ".$arrHuongDan[$maNoiDung]." )";
    }
    $html.="<div class='row justify-content-center mt-3'>";
    $html.="<div class='div-content'>";
    $html.="<div class='div-content-title'><h5>".$stt++." - ".$arrNoiDung[$maNoiDung]."</h5></div>";
    //Huong dan
    $html.="<div class='div-content-title'><i>".$arrHuongDan[$maNoiDung]."</i></div>";
    //Diem chuan
    $html.="<div class='form-group row mt-3'>";
    $html.="<label for='input-DiemChuan-".$maNoiDung."' class='col-sm-4 col-form-label offset-sm-2 mb-2'>Điểm chuẩn</label>";
    $html.="<div class='col-sm-3'>";
    $html.="<input type='number' class='form-control' id='input-DiemChuan-".$maNoiDung."' value='".$arrTrongSoDiem[$maNoiDung]."' readonly>";
    $html.="</div>";
    $html.="</div>";
    //Diem tu nhap
    $html.="<div class='form-group row'>";
    $html.="<label for='input-".$maNoiDung."' class='col-sm-4 col-form-label offset-sm-2 mb-2'>Điểm tự chấm</label>";
    $html.="<div class='col-sm-3'>";
    $html.="<input type='number' class='form-control inputDiemTuNhap' id='input-".$maNoiDung."' value='' min='0' max='".$arrTrongSoDiem[$maNoiDung]."' readonly>";
    $html.="</div>";
    $html.="</div>";
    //Diem đơn vị chấm
    $html.="<div class='form-group row inputReadOnly'>";
    $html.="<label for='input-DVC-".$maNoiDung."' class='col-sm-4 col-form-label offset-sm-2 mb-2'>Điểm đơn vị chấm</label>";
    $html.="<div class='col-sm-3'>";
    $html.="<input type='number' class='form-control' id='input-DVC-".$maNoiDung."' value='' readonly>";
    $html.="</div>";
    $html.="</div>";
    //Điểm phúc tra
    $html.="<div class='form-group row inputReadOnly'>";
    $html.="<label for='input-DPT-".$maNoiDung."' class='col-sm-4 col-form-label offset-sm-2'>Điểm phúc tra</label>";
    $html.="<div class='col-sm-3'>";
    $html.="<input type='number' class='form-control' id='input-DPT-".$maNoiDung."' value='' readonly>";
    $html.="</div>";
    $html.="</div>";
    //End
    $html.="</div>";
    $html.="</div>";
}


?>