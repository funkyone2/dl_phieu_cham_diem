<?php
if (!isset($_SESSION)) {
    session_start();
}
if (!isset($_SESSION['maHR'])) {
    header("Location:./login.php");
}
?>
<!DOCTYPE html>
<html>
<?php

include_once("./head.php");

?>
<link rel="stylesheet" href="../css/changepassword.css">

<body>
    <nav class="navbar navbar-light bg-light">
        <span class="navbar-brand mb-0 h1">
            <?php
            echo $_SESSION['infoUser']["TenDonViCap3"] . " - " . $_SESSION['infoUser']['TenDonViCap4'];
            ?>
        </span>
        <div class="btn-group">
            <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <?php echo $_SESSION['maHR']; ?>
            </button>
            <div class="dropdown-menu dropdown-menu-right dropdown-custome">
                <button class="dropdown-item" type="button" id="btnLogout">Đăng xuất</button>
            </div>
        </div>
    </nav>

    <div class="container">
        
            <div class="div-title"><h2 class="text-center mt-3 mb-3">Đổi mật khẩu</h2></div>
            <div class="row justify-content-center">
                <form class="div-content" id="frmSave" action="./changepassController.php" method="post">
                    <div class="form-group row justify-content-center">
                        <label for="inputPass" class="col-sm-4 col-form-label">Mật khẩu cũ</label>
                        <div class="col-sm-6">
                            <input type="password" class="form-control" id="inputPass" name="passwd" placeholder="Nhập mật khẩu">
                        </div>
                    </div>
                    <div class="form-group row justify-content-center">
                        <label for="inputPassNew" class="col-sm-4 col-form-label">Mật khẩu mới</label>
                        <div class="col-sm-6">
                            <input type="password" class="form-control" id="inputPassNew" name="passwdNew" placeholder="Nhập mật khẩu">
                        </div>
                    </div>
                    <div class="form-group row justify-content-center">
                        <label for="inputPassNewAgain" class="col-sm-4 col-form-label">Nhập lại mật khẩu mới</label>
                        <div class="col-sm-6">
                            <input type="password" class="form-control" id="inputPassNewAgain" name="passNewAgain" placeholder="Nhập mật khẩu">
                        </div>
                    </div>
                    <div class="form-group row justify-content-center mt-4">
                        <input type="button" class="btn btn-success col-sm-2" id="btnCancel" value="Quay lại"></input>
                        <input type="button" class="btn btn-primary col-sm-2 offset-sm-1" id="btnSave" value="Lưu"></input>
                    </div>
                </form>
            </div>
        
    </div>
</body>
<form id="frmLogout" action="./backerror.php" method="post"></form>
<form id="frmCancel" action="./chamdiem.php" method="post"></form>
<?php include_once("js_include.php"); ?>
<script style="text/javascript" src="../js/changepassword.js"></script>

</html>