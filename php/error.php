<?php
     if(!isset($_SESSION)){
        session_start();
      }
    if(!isset($_SESSION['error_info'])){
        header("Location:./login.php");
    }
?>
<!DOCTYPE html>
<html>
<!-- head -->
<?php include_once("head.php"); ?>
<link rel="stylesheet" href="../css/error.css">
<!-- /head -->


<body>
    <!-- header -->
    <?php include_once("header.php"); ?>
    <!-- /header -->
    <div class="container">
        <div class="text-center"><h1><?php echo $_SESSION['error_info'];?></h1></div>
        <button type="button" class="btn btn-primary btn-back" id="btnBack">Quay lại</button>
    </div>
    <form id="frmBack" action="./backerror.php" method="POST"></form>
</body>
<?php include_once("js_include.php"); ?>
<script style="text/javascript" src="../js/error.js"></script>
</html>