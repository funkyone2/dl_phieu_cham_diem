<?php 
  if(!isset($_SESSION)){
    session_start();
  }
if (isset($_SESSION['maHR'])) {
    header("Location:./chamdiem.php");
}
?>
<!DOCTYPE html>
<html>
<!-- head -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>
    Công ty Điện lực Bắc Giang
    </title>
    <link rel="stylesheet" href="../lib/Bootstrap/css/bootstrap.min.css">
</head>
<!-- /head -->
<link rel="stylesheet" href="../css/login.css">

<body>
    <!-- header -->
    <nav class="navbar navbar-light bg-light">
        <span class="navbar-brand mb-0 h1">Công ty Điện lực Bắc Giang</span>
    </nav>
    <!-- /header -->
    <div class="container">
        <div class="parent">
            <div class="login-form-cus">
                <div id="form-header">
                    Đăng nhập
                </div>
                <img src="../image/logo_2.jpg" class="float-left ml-sm-3 mt-3" width="200" height="200" alt="...">
                <form class="login-form float-left mt-1" id="frmLogin" method="post" action="./authenticate.php">

                    <div class="form-group">
                        <label for="input_maHR">Mã HR</label>
                        <input type="input" class="form-control" id="input_maHR" placeholder="Mã HR" name="username">
                    </div>
                    <div class="form-group">
                        <label for="input_pass">Password</label>
                        <input type="password" class="form-control" id="input_pass" placeholder="Mật khẩu" name="password">
                    </div>
                    <button type="submit" class="btn btn-primary btn-login">Đăng nhập</button>
                </form>
            </div>
        </div>
    </div>
</body>
<?php include_once("js_include.php"); ?>

</html>